package schule.aufgaben;

import java.awt.Color;
import java.util.Scanner;

public class Taschenrechner {

	
	public static void addition() {
		double zahl1;
		double zahl2;
		double ergebnis;
		
		Scanner scannerVariable = new Scanner(System.in);
		
		System.out.println("-<----------->-");
		System.out.println("");
		System.out.println("Addition-Taschenrechner:");
		System.out.println("");
		System.out.println("Erste Zahl: ");
		zahl1 = scannerVariable.nextDouble();
		System.out.println("Zweite Zahl: ");
		zahl2 = scannerVariable.nextDouble();
		ergebnis = zahl1 + zahl2;
		System.out.println("Ergebnis: " + ergebnis);
		System.out.println("-<----------->-");
	}
}
