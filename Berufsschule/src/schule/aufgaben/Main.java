package schule.aufgaben;

import schule.aufgaben.Konsolenausgaben;

public class Main {

	public static void main(String[] args) {
		System.out.println("Main-Class erfolgreich gestartet!");
		Konsolenausgaben.consoleOutput();
		Taschenrechner.addition();
	}

}
